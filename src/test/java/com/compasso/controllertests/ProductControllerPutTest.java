package com.compasso.controllertests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@SpringBootTest
public class ProductControllerPutTest {
	
	private final static String POST_URI = "/products";
	
	private final static String PUT_URI = "/products/{id}";
	
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void givenProductsPut_whenMockMVC_thenVerifyResponse() throws Exception {
    	
    	this.saveProductBeforUpdateTest();
    	
    	this.mockMvc.perform(put(PUT_URI, "1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"Samsung Book X30\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": 3000.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().isOk());
    }

    @Test
    public void givenProductsPut_whenMockMVC_withNotFoundProduct_thenMustFail() throws Exception {
    	
    	this.mockMvc.perform(put(PUT_URI, "9000")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"Samsung Book X30\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": 3000.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPut_whenMockMVC_withWrongUri_thenMustFail() throws Exception {

    	this.saveProductBeforUpdateTest();
    	
    	this.mockMvc.perform(put("/prod/{id}", "1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"Samsung\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": 10.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPut_whenMockMVC_withWrongParam_thenMustFail() throws Exception {

    	this.saveProductBeforUpdateTest();
    	
    	this.mockMvc.perform(put(PUT_URI, "ABC")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"Samsung\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": 10.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    
    @Test
    public void givenProductsPut_whenMockMVC_withWrongContentType_thenMustFail() throws Exception {
    	
    	this.saveProductBeforUpdateTest();

    	this.mockMvc.perform(put(PUT_URI, "1")
            .contentType(MediaType.APPLICATION_XML)
            .content("{\n"
            		+ "    \"name\": \"Samsung\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": 10.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPut_whenMockMVC_withCharacterPrice_thenMustFail() throws Exception {
    	
    	this.saveProductBeforUpdateTest();
    	
    	this.mockMvc.perform(put(PUT_URI, "1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"Samsung\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": TEN\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPut_whenMockMVC_withNegativePrice_thenMustFail() throws Exception {
    	
    	this.saveProductBeforUpdateTest();

    	this.mockMvc.perform(put(PUT_URI, "1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"Samsung\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": -10.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    

    @Test
    public void givenProductsPut_whenMockMVC_withEmptyProductName_thenMustFail() throws Exception {

    	this.saveProductBeforUpdateTest();
    	
    	this.mockMvc.perform(put(PUT_URI, "1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": 100.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPut_whenMockMVC_withEmptyProductDescription_thenMustFail() throws Exception {
    	
    	this.saveProductBeforUpdateTest();

    	this.mockMvc.perform(put(PUT_URI, "1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"Samsung\",\n"
            		+ "    \"description\": \"\",\n"
            		+ "    \"price\": 10.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPut_whenMockMVC_withWrongPayload_thenMustFail() throws Exception {

    	this.saveProductBeforUpdateTest();

    	this.mockMvc.perform(put(PUT_URI, "1")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"description\": \"\",\n"
            		+ "    \"price\": -10.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPut_whenMockMVC_withoutPayload_thenMustFail() throws Exception {
    	
    	this.saveProductBeforUpdateTest();
    	
    	this.mockMvc.perform(put(PUT_URI, "1")
            .contentType(MediaType.APPLICATION_JSON)
            .content(""))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
	private void saveProductBeforUpdateTest() throws Exception {
		this.mockMvc.perform(post(POST_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n"
                		+ "    \"name\": \"Samsung\",\n"
                		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
                		+ "    \"price\": 10.00\n"
                		+ "}"));
	}

}
