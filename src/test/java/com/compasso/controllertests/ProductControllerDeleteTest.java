package com.compasso.controllertests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@SpringBootTest
public class ProductControllerDeleteTest {
	
	private final static String POST_URI = "/products";
	
	private final static String DELETE_URI = "/products/{id}";
	
    @Autowired
    private MockMvc mockMvc;
	
    
    @Test
    public void givenProductsDelete_whenMockMVC_thenVerifyResponse() throws Exception {
    	
    	this.saveProductBeforDeleteTest();
    	
    	this.mockMvc.perform(delete(DELETE_URI, "1")
            .contentType(MediaType.APPLICATION_JSON))
    		.andDo(print())
            .andExpect(status().isOk());
    }
    
    @Test
    public void givenProductsDelete_whenMockMVC_withNotFoundProduct_thenMustFail() throws Exception {
    	
    	this.mockMvc.perform(delete(DELETE_URI, "1999")
            .contentType(MediaType.APPLICATION_JSON))
    		.andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsDelete_whenMockMVC_withWrongUri_thenMustFail() throws Exception {

    	this.saveProductBeforDeleteTest();
    	
    	this.mockMvc.perform(delete("/prod/{id}", "1")
            .contentType(MediaType.APPLICATION_JSON))
    		.andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsDelete_whenMockMVC_withWrongParam_thenMustFail() throws Exception {

    	this.saveProductBeforDeleteTest();
    	
    	this.mockMvc.perform(delete(DELETE_URI, "ABC")
            .contentType(MediaType.APPLICATION_JSON))
    		.andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    
	private void saveProductBeforDeleteTest() throws Exception {
		this.mockMvc.perform(post(POST_URI)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n"
                		+ "    \"name\": \"Samsung\",\n"
                		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
                		+ "    \"price\": 10.00\n"
                		+ "}"));
	}
    

}
