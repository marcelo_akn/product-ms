package com.compasso.controllertests;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.compasso.api.dto.input.ProductDtoInput;
import com.fasterxml.jackson.databind.ObjectMapper;

@AutoConfigureMockMvc
@SpringBootTest
public class ProductControllerGetTest {
	
	private final static String POST_URI = "/products";
	
	private final static String GET_URI = "/products";
	
	private final static String GET_URI_ID = GET_URI + "/{id}";
	
	
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private ObjectMapper objectMapper;

    
    @Test
    public void givenProductsGetAll_whenMockMVC_thenVerifyResponse() throws Exception {
    	
    	List<ProductDtoInput> products = this.generateListOfProducts(10);
    	
    	this.saveProductBeforeGetTest(products);
    	
        this.mockMvc.perform(get(GET_URI)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(ProductDtoInput.builder().build())))
	        //.andDo(print())
            .andExpect(jsonPath("$.size()", is(products.size())))
            .andExpect(status().isOk());
        
    }
    
    
    @Test
    public void givenProductGetById_whenMockMVC_thenVerifyResponse() throws Exception {
    	
    	List<ProductDtoInput> products = this.generateListOfProducts(1);
    	
    	this.saveProductBeforeGetTest(products);
    	
        this.mockMvc.perform(get(GET_URI_ID, 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(
            		ProductDtoInput.builder().build())))
	        //.andDo(print())
            .andExpect(jsonPath("$.name", is(products.get(0).getName())))
            .andExpect(status().isOk());
    	
    }
    
    
    @Test
    public void givenProductGetById_whenMockMVC_thenMustFail() throws Exception {
    	
    	List<ProductDtoInput> products = this.generateListOfProducts(1);
    	
    	this.saveProductBeforeGetTest(products);
    	
        this.mockMvc.perform(get(GET_URI_ID, 999)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(
            		ProductDtoInput.builder().build())))
        	//.andDo(print())
            .andExpect(status().is4xxClientError());
    	
    }
    

    @Test
    public void givenProductGetByIdWithWrongUri_whenMockMVC_thenMustFail() throws Exception {
    	
    	List<ProductDtoInput> products = this.generateListOfProducts(1);
    	
    	this.saveProductBeforeGetTest(products);
    	
        this.mockMvc.perform(get("/produtoo/", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(
            		ProductDtoInput.builder().build())))
	        //.andDo(print())
            .andExpect(status().is4xxClientError());
    	
    }
    
    
    @Test
    public void givenProductGetByIdWithWrongPathVariable_whenMockMVC_thenMustFail() throws Exception {
    	
    	List<ProductDtoInput> products = this.generateListOfProducts(1);
    	
    	this.saveProductBeforeGetTest(products);
    	
        this.mockMvc.perform(get(GET_URI_ID, "ABC")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(
            		ProductDtoInput.builder().build())))
	        //.andDo(print())
            .andExpect(status().is4xxClientError());
    	
    }


    @Test
    public void givenProductGetByIdWithoutURI_whenMockMVC_thenMustFail() throws Exception {
    	
    	List<ProductDtoInput> products = this.generateListOfProducts(1);
    	
    	this.saveProductBeforeGetTest(products);
    	
        this.mockMvc.perform(get("/", 1)
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(
            		ProductDtoInput.builder().build())))
	        //.andDo(print())
            .andExpect(status().is4xxClientError());
    	
    }
    
    
    @Test
    public void givenProductGetByPriceParam_whenMockMVC_thenVerifyResponse() throws Exception {
    	
    	List<ProductDtoInput> products = this.generateListOfProducts(10);
    	
    	this.saveProductBeforeGetTest(products);
    	
        this.mockMvc.perform(get("/products/search")
        		.param("min_price", "11.00")
        		.param("max_price", "50.00")
        		.param("q", "Product 1"))
        	.andDo(print())
            .andExpect(status().isOk());
    	
    }
    
    
	private void saveProductBeforeGetTest(List<ProductDtoInput> products) throws Exception {
		
		products.forEach(p -> {
			try {
				String json = this.objectMapper.writeValueAsString(p);
				
				this.mockMvc.perform(post(POST_URI)
						.contentType(MediaType.APPLICATION_JSON)
						.content(json));
				
			} catch (Exception e) {
				e.printStackTrace();
				
			}
		});
		
	}

	private List<ProductDtoInput> generateListOfProducts(Integer numberProducts) {
		
		List<ProductDtoInput> products = new ArrayList<>();
		
		Integer i = 1;
		
		while (i <= numberProducts) {
			products.add(
				ProductDtoInput.builder
					().name("Product " + i)
					.description("Product " + i + " description")
					.price(BigDecimal.TEN.multiply(BigDecimal.valueOf(i)))
					.build());
			i++;
		}
		
		return products;
	}

}
