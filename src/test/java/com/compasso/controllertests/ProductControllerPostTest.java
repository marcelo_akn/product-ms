package com.compasso.controllertests;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@SpringBootTest
public class ProductControllerPostTest {
	
	private final static String POST_URI = "/products";
	
    @Autowired
    private MockMvc mockMvc;
    
    
    @Test
    public void givenProductsPost_whenMockMVC_thenVerifyResponse() throws Exception {
    	this.mockMvc.perform(post(POST_URI)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"Samsung\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": 10.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().isCreated());
    }

    @Test
    public void givenProductsPost_whenMockMVC_withZeroPrice_thenVerifyResponse() throws Exception {
    	this.mockMvc.perform(post(POST_URI)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"Samsung\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": 0\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().isCreated());
    }
    
    @Test
    public void givenProductsPost_whenMockMVC_withWrongUri_thenMustFail() throws Exception {
    	this.mockMvc.perform(post("/productss")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"Samsung\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": 10.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPost_whenMockMVC_withWrongContentType_thenMustFail() throws Exception {
    	this.mockMvc.perform(post("/productss")
            .contentType(MediaType.APPLICATION_XML)
            .content("{\n"
            		+ "    \"name\": \"Samsung\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": 10.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPost_whenMockMVC_withCharacterPrice_thenMustFail() throws Exception {
    	this.mockMvc.perform(post(POST_URI)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"Samsung\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": TEN\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPost_whenMockMVC_withNegativePrice_thenMustFail() throws Exception {
    	this.mockMvc.perform(post(POST_URI)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"Samsung\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": -10.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPost_whenMockMVC_withEmptyProductName_thenMustFail() throws Exception {
    	this.mockMvc.perform(post(POST_URI)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"\",\n"
            		+ "    \"description\": \"Notebook Samsung Book X30, processador Intel I3\",\n"
            		+ "    \"price\": 100.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPost_whenMockMVC_withEmptyProductDescription_thenMustFail() throws Exception {
    	this.mockMvc.perform(post(POST_URI)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"name\": \"Samsung\",\n"
            		+ "    \"description\": \"\",\n"
            		+ "    \"price\": 10.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPost_whenMockMVC_withWrongPayload_thenMustFail() throws Exception {
    	this.mockMvc.perform(post(POST_URI)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{\n"
            		+ "    \"description\": \"\",\n"
            		+ "    \"price\": -10.00\n"
            		+ "}"))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
    
    @Test
    public void givenProductsPost_whenMockMVC_withoutPayload_thenMustFail() throws Exception {
    	this.mockMvc.perform(post(POST_URI)
            .contentType(MediaType.APPLICATION_JSON)
            .content(""))
            .andDo(print())
            .andExpect(status().is4xxClientError());
    }
}
