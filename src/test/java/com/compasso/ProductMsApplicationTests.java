package com.compasso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.compasso.api.dto.input.ProductDtoInput;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
class ProductMsApplicationTests {
	
    @Autowired
    private ObjectMapper objectMapper;

	@Test
	void contextLoads() {
		List<ProductDtoInput> products = new ArrayList<>();

		Integer i = 1;
		while (i < 11) {
			products.add(ProductDtoInput.builder()
					.name("Product " + i)
					.description("Product description " + i)
					.price(BigDecimal.TEN.multiply(BigDecimal.valueOf(i))).build());
			i++;
		}
		
		products.forEach(p -> {
			try {
				String json = this.objectMapper.writeValueAsString(p);
				System.out.println(json);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}

}
