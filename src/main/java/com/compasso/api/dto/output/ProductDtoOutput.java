package com.compasso.api.dto.output;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "Product output", description = "Representation of an output product")
@Getter
@Setter
public class ProductDtoOutput {
	
	private String id;
	
	private String name;
	
	private String description;
	
	private BigDecimal price;

}
