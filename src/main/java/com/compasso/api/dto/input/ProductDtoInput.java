package com.compasso.api.dto.input;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

import org.hibernate.validator.constraints.Length;

import io.swagger.annotations.ApiModel;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@ApiModel(value = "Product input", description = "Representation of an input product")
@Getter
@Setter
@Builder
public class ProductDtoInput {
	
	@NotNull
	@NotBlank(message = "Enter the product name")
	@Length(min = 2, message = "The product name must be longer than two characters")
	private String name;
	
	@NotNull
	@NotBlank(message = "Enter the product description")
	@Length(min = 2, message = "The product description must be longer than two characters")
	private String description;
	
	@NotNull(message = "Enter the product price")
	@PositiveOrZero(message = "The product price must be greater than or equal to zero")
	private BigDecimal price;

}
