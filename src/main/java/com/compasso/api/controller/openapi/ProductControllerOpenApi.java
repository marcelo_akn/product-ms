package com.compasso.api.controller.openapi;

import java.math.BigDecimal;
import java.util.List;

import com.compasso.api.dto.input.ProductDtoInput;
import com.compasso.api.dto.output.ProductDtoOutput;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(tags = "Products")
public interface ProductControllerOpenApi {

	@ApiOperation("Save a product")
	ProductDtoOutput save(
		@ApiParam(name = "payload", value = "Product representation without ID")
		ProductDtoInput productDtoInput);

	@ApiOperation("Get all products from database")
	List<ProductDtoOutput> getAllProducts();

	@ApiOperation("Find a product by ID")
	ProductDtoOutput getProductById(
		@ApiParam(value = "Product ID - application genereted code", example = "1")
		Long id);

	@ApiOperation("Find products by name, min and max price")
	List<ProductDtoOutput> getAllProductsParam(
		@ApiParam(name = "Min. price", value = "The minimum value of the product", example = "1")
		BigDecimal min_price, 
		@ApiParam(name = "Man. price", value = "The máximum value of the product", example = "1")
		BigDecimal max_price, 
		@ApiParam(name = "Name/Description", value = "Name or description of the product")
		String q);

	@ApiOperation("Update product information")
	ProductDtoOutput update(
		@ApiParam(value = "Product ID - application genereted code", example = "1")
		Long id,
		@ApiParam(name = "payload", value = "Product representation without ID")
		ProductDtoInput productDtoInput);

	@ApiOperation("Delete product by ID")
	void delete(
		@ApiParam(value = "Product ID - application genereted code", example = "1")
		Long id);

}