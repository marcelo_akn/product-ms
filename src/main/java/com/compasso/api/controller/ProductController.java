package com.compasso.api.controller;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.compasso.api.assembler.ProductDtoAssembler;
import com.compasso.api.controller.openapi.ProductControllerOpenApi;
import com.compasso.api.dto.input.ProductDtoInput;
import com.compasso.api.dto.output.ProductDtoOutput;
import com.compasso.domain.model.Product;
import com.compasso.domain.service.ProductService;


@RestController
@RequestMapping("/products")
public class ProductController implements ProductControllerOpenApi {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductDtoAssembler productDtoAssembler;
	
	
	@Override
	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ProductDtoOutput save(@RequestBody @Valid ProductDtoInput productDtoInput) {
		
		Product product = this.productDtoAssembler.toDomainObjectFromDto(productDtoInput);
		
		return this.productDtoAssembler.toDtoFromModel(this.productService.save(product));
	}
	
	
	@Override
	@GetMapping
	public List<ProductDtoOutput> getAllProducts() {
		
		return this.productDtoAssembler.toCollectionDtoFromModel(
				this.productService.findAllProductsOrderByName());
	}
	
	
	@Override
	@GetMapping("/{id}")
	public ProductDtoOutput getProductById(@PathVariable Long id) {
		
		return this.productDtoAssembler.toDtoFromModel(this.productService.findById(id));
	}
	
	@Override
	@GetMapping("/search")
	public List<ProductDtoOutput> getAllProductsParam(
		@RequestParam BigDecimal min_price, 
		@RequestParam BigDecimal max_price,
		@RequestParam String q) {
		
		return this.productDtoAssembler.toCollectionDtoFromModel(
				this.productService.findAllByProductsParam(q, min_price, max_price));
	}
	
	@Override
	@PutMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public ProductDtoOutput update(
		@PathVariable Long id, 
		@RequestBody @Valid ProductDtoInput productDtoInput) {
		
		Product product = this.productService.findById(id);
		
		this.productDtoAssembler.copyToDomainObjectFromDto(productDtoInput, product);
		
		return this.productDtoAssembler.toDtoFromModel(this.productService.update(product));
	}
	
	
	@Override
	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.OK)
	public void delete(@PathVariable Long id) {
		
		Product product = this.productService.findById(id);
		
		this.productService.delete(product.getId());
	}

}
