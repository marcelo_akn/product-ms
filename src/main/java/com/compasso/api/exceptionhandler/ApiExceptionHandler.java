package com.compasso.api.exceptionhandler;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.TypeMismatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.Nullable;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.compasso.domain.exception.BusinessException;
import com.compasso.domain.exception.EntityAlreadySavedException;
import com.compasso.domain.exception.EntityNotFoundException;
import com.compasso.domain.exception.ObjectException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MessageSource messageSource;
	
	
	@Override
	protected ResponseEntity<Object> handleNoHandlerFoundException(
			NoHandlerFoundException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		String detail = String.format("The resource %s not found.", 
	            ex.getRequestURL());
		
		return handleExceptionInternal(ex, detail, 
				new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
	
	
	@Override
	protected ResponseEntity<Object> handleTypeMismatch(
			TypeMismatchException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		
		if (ex instanceof MethodArgumentTypeMismatchException) {
			return handleMethodArgumentTypeMismatch(
					(MethodArgumentTypeMismatchException) ex, headers, status, request);
		}
	
		return super.handleTypeMismatch(ex, headers, status, request);
	}
	
	
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(
			HttpMessageNotReadableException ex, HttpHeaders headers, 
			HttpStatus status, WebRequest request) {
		
		Throwable rootCause = ExceptionUtils.getRootCause(ex);
		
		if (rootCause instanceof InvalidFormatException) {
			return handleInvalidFormatException(
				(InvalidFormatException)rootCause, headers, status, request);
		} 
		
		return handleExceptionInternal(ex, "Invalid payload", 
				new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
	

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers, 
			HttpStatus status, WebRequest request) {
		
	    return handleValidationInternal(ex, ex.getBindingResult(), new HttpHeaders(), 
	            HttpStatus.BAD_REQUEST, request);
	}
	
	
	@ExceptionHandler(BusinessException.class)
	public ResponseEntity<?> catchBusinessException(BusinessException e, 
			WebRequest request){
		
		HttpStatus status = HttpStatus.BAD_REQUEST;
		
		return handleExceptionInternal(e, e.getMessage(), new HttpHeaders(), 
				status, request);
	}
	
	
	@ExceptionHandler(ObjectException.class)
	public ResponseEntity<?> catchObjectException(ObjectException e, 
			WebRequest request){
		
		HttpStatus status = HttpStatus.BAD_REQUEST;
		
		return handleExceptionInternal(e, e.getMessage(), new HttpHeaders(), 
				status, request);
	}
	
	
	@ExceptionHandler(EntityAlreadySavedException.class)
	public ResponseEntity<?> catchEntityAlreadySavedException(EntityAlreadySavedException e, 
			WebRequest request){
		
		HttpStatus status = HttpStatus.CONFLICT;
		
		return handleExceptionInternal(e, e.getMessage(), new HttpHeaders(), 
				status, request);
	}
	

	@ExceptionHandler(EntityNotFoundException.class)
	public ResponseEntity<?> catchEntityNotFoundException(EntityNotFoundException e, 
			WebRequest request){
		
		HttpStatus status = HttpStatus.NOT_FOUND;
		
		return handleExceptionInternal(e, e.getMessage(), new HttpHeaders(), 
				status, request);
	}
	
	
	public ResponseEntity<Object> handleValidationInternal(Exception ex, BindingResult bindingResult,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		List<ApiError.Object> problemObjects = bindingResult.getAllErrors()
				.stream()
				.map(objectError -> {
					String message = messageSource.getMessage(objectError, LocaleContextHolder.getLocale());
					
					String name = objectError.getObjectName();
					
					if (objectError instanceof FieldError) {
						name = ((FieldError) objectError).getField();
					}
					
					return ApiError.Object.builder()
						.name(name)
						.userMessage(message)
						.build();
					})
				.collect(Collectors.toList());
		
		String userMessage = getErrorBeanValidationMessage(problemObjects);
		
		ApiError apiError = this.createApiError(status, userMessage);

		return handleExceptionInternal(ex, apiError, headers, status, request);
	}
	
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleUncaught(Exception ex, WebRequest request) {
	    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;		

	    String detail = "Internal server error. Please, contact the system administrator.";

	    return handleExceptionInternal(ex, detail, new HttpHeaders(), status, request);
	}           	
	
	
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(
			Exception ex, @Nullable Object body, HttpHeaders headers, 
			HttpStatus status, WebRequest request) {
		
		body = this.generateBody(body, status);

		return super.handleExceptionInternal(ex, body, headers, status, request);
	}
	
	
	private ResponseEntity<Object> handleMethodArgumentTypeMismatch(
			MethodArgumentTypeMismatchException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {

		String detail = String.format("The parameter URL '%s' got '%s', "
				+ " and it's invalid type. Fix and put a valid type (%s).",
				ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName());

		return handleExceptionInternal(ex, detail, headers, status, request);
	}

	
	private ResponseEntity<Object> handleInvalidFormatException(
			InvalidFormatException ex, HttpHeaders headers,
			HttpStatus status, WebRequest request) {
		
		String path = ex.getPath().stream()
				.map(ref -> ref.getFieldName())
				.collect(Collectors.joining("."));
		
		String detail = String.format("The field '%s' got the value '%s', "
				+ "and it is invalid. Fix and put a valid value (%s).",
				path, ex.getValue(), ex.getTargetType().getSimpleName());
		
		return handleExceptionInternal(ex, detail, headers, status, request);
	}
	

	private String getErrorBeanValidationMessage(List<ApiError.Object> problemObjects) {
		String userMessage = "";
		String userMessages = "";
		
		if (problemObjects != null && problemObjects.size() > 0) {
			userMessages = problemObjects
					.stream()
					.filter(m -> m.getUserMessage() != null && !m.getUserMessage().trim().isEmpty())
					.map(m -> m.getUserMessage().trim())
					.collect(Collectors.joining(", "));
		}
		
		if (userMessages != "") {
			userMessage = userMessages;
		} else {
			userMessage = "Invalid fields. Fix it and try again.";
		}
		
		return userMessage;
	}
	
	
	private Object generateBody(Object body, HttpStatus status) {
		if (body == null || (body != null && body instanceof String)) {
			String exceptionMessage = "";
			
			if (body == null) {
				exceptionMessage = status.getReasonPhrase();
			} else {
				exceptionMessage = (String) body;
			}
			
			body = this.createApiError(status, exceptionMessage);
		}
		return body;
	}
	
	
	private ApiError createApiError(HttpStatus status, String detail) {
		
		return ApiError.builder()
				.status_code(status.value())
				.message(detail)
				.build();
	}

}
