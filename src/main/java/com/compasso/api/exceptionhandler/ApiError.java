package com.compasso.api.exceptionhandler;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ApiError {
	
	@ApiModelProperty(example = "400")
	private Integer status_code;
	
	private String message;
	
	@Getter
	@Builder
	public static class Object {
		
		private String name;
		
		private String userMessage;
	}

}
