package com.compasso.api.assembler;

import org.springframework.stereotype.Component;

import com.compasso.api.dto.input.ProductDtoInput;
import com.compasso.api.dto.output.ProductDtoOutput;
import com.compasso.domain.model.Product;

@Component
public class ProductDtoAssembler extends GenericDtoAssembler<Product, ProductDtoInput, ProductDtoOutput>{

}
