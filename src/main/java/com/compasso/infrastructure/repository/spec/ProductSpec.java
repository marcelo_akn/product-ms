package com.compasso.infrastructure.repository.spec;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.compasso.domain.model.Product;

public class ProductSpec {
	
	public static Specification<Product> findByAttributes(String q, BigDecimal minPrice, BigDecimal maxPrice) {
		return (root, query, criteriaBuilder) -> {
			
			List<Predicate> predicates = createPredicates(q, minPrice, maxPrice, root, criteriaBuilder);
			
			return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
		};
	}

	private static List<Predicate> createPredicates(String q, 
			BigDecimal minPrice, BigDecimal maxPrice, 
			Root<Product> root,	CriteriaBuilder criteriaBuilder) {
		
		List<Predicate> predicates = new ArrayList<>();
			
		if (q != null && !q.trim().isEmpty()) {
			predicates.add(
				criteriaBuilder.or(
					criteriaBuilder.equal(criteriaBuilder.upper(criteriaBuilder.trim(root.get("name"))),q.trim().toUpperCase()), 
					criteriaBuilder.equal(criteriaBuilder.upper(criteriaBuilder.trim(root.get("description"))),	q.trim().toUpperCase())));
		}
		
		if (minPrice != null) {
			predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("price"), minPrice));
		}
		
		if (maxPrice != null) {
			predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get("price"), maxPrice));
		}
		
		return predicates;
	}

}
