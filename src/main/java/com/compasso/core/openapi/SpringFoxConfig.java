package com.compasso.core.openapi;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.compasso.api.exceptionhandler.ApiError;
import com.fasterxml.classmate.TypeResolver;

import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SpringFoxConfig implements WebMvcConfigurer {
	
	@Bean
	public Docket apiDocket() {
		TypeResolver typeResolver = new TypeResolver(); 
		
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.compasso.api"))
				.build()
			.useDefaultResponseMessages(false)
			.globalResponseMessage(RequestMethod.GET, this.globalGetResponseMessages())
			.globalResponseMessage(RequestMethod.POST, this.globalPostPutResponseMessages())
			.globalResponseMessage(RequestMethod.PUT, this.globalPostPutResponseMessages())
			.globalResponseMessage(RequestMethod.DELETE, this.globalDeleteResponseMessages())
			.additionalModels(typeResolver.resolve(ApiError.class))
			.apiInfo(this.apiInfo())
			.tags(new Tag("Products", "Create, read, update and delete products. Search a list of products, by ID or by name, min and max price."));
	}
	
	public ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("product-ms")
				.description("Compasso Uol - Desafio técnico")
				.version("1")
				.contact(new Contact("Marcelo Akio Nishimori", "https://www.linkedin.com/in/marcelo-akio-nishimori-1907919b/", "marcelo.akn@gmail.com"))
				.build();
	}
	
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/swagger-ui.html")
			.addResourceLocations("classpath:/META-INF/resources/");
		
		registry.addResourceHandler("/webjars/**")
			.addResourceLocations("classpath:/META-INF/resources/webjars/");
	}
	
	
	private List<ResponseMessage> globalGetResponseMessages() {
		return Arrays.asList(
				new ResponseMessageBuilder()
					.code(HttpStatus.NOT_FOUND.value())
					.message("Not found")
					.responseModel(new ModelRef("ApiError"))
					.build(),
				new ResponseMessageBuilder()
					.code(HttpStatus.NOT_ACCEPTABLE.value())
					.message("Resource has no representation that could be accepted by the consumer")
					.build(),
				new ResponseMessageBuilder()
					.code(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.message("Internal server error")
					.build()
			);
	}
	
	private List<ResponseMessage> globalPostPutResponseMessages() {
		return Arrays.asList(
				new ResponseMessageBuilder()
					.code(HttpStatus.NOT_FOUND.value())
					.message("Not found")
					.responseModel(new ModelRef("ApiError"))
					.build(),
				new ResponseMessageBuilder()
					.code(HttpStatus.BAD_REQUEST.value())
					.message("Bad request. Invalid request")
					.responseModel(new ModelRef("ApiError"))
					.build(),
				new ResponseMessageBuilder()
					.code(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.message("Internal server error")
					.responseModel(new ModelRef("ApiError"))
					.build(),
				new ResponseMessageBuilder()
					.code(HttpStatus.NOT_ACCEPTABLE.value())
					.message("Resource has no representation that could be accepted by the consumer")
					.build(),
				new ResponseMessageBuilder()
					.code(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value())
					.message("Request declined because the body is in an unsupported format")
					.responseModel(new ModelRef("ApiError"))
					.build()
			);
	}
	
	private List<ResponseMessage> globalDeleteResponseMessages() {
		return Arrays.asList(
				new ResponseMessageBuilder()
					.code(HttpStatus.BAD_REQUEST.value())
					.message("Bad request. Invalid request")
					.responseModel(new ModelRef("ApiError"))
					.build(),
				new ResponseMessageBuilder()
					.code(HttpStatus.INTERNAL_SERVER_ERROR.value())
					.message("Internal server error")
					.build()
			);
	}
	

}
