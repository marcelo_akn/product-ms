package com.compasso.domain.exception;

public class ObjectException extends BusinessException {

	private static final long serialVersionUID = 1L;
	
	public ObjectException(String message) {
		super(message);
	}

}
