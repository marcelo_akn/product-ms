package com.compasso.domain.exception;

public class EntityNotFoundException extends BusinessException {

	private static final long serialVersionUID = 1L;
	
	public EntityNotFoundException(String message) {
		super(message);
	}

	public EntityNotFoundException(Long id) {
		super(String.format("Product not found with ID %d", id));
	}
}
