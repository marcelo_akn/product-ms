package com.compasso.domain.exception;

public class EntityAlreadySavedException extends BusinessException {

	private static final long serialVersionUID = 1L;
	
	public EntityAlreadySavedException(String message) {
		super(message);
	}

}
