package com.compasso.domain.service;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.compasso.domain.exception.EntityAlreadySavedException;
import com.compasso.domain.exception.EntityNotFoundException;
import com.compasso.domain.exception.ObjectException;
import com.compasso.domain.model.Product;
import com.compasso.domain.repository.ProductRepository;
import com.compasso.infrastructure.repository.spec.ProductSpec;

@Service
public class ProductService {
	
	@Autowired
	private ProductRepository productRepository;
	
	
	@Transactional
	public Product save(Product product) {
		
		this.validateProductToSave(product);
		
		return productRepository.save(product);
	}
	
	
	public Product findById(Long id) {
		
		return productRepository.findById(id)
				.orElseThrow(() -> new EntityNotFoundException(id));
	}
	
	
	public List<Product> findAllProductsOrderByName() {
		
		return productRepository.findAll()
				.stream()
				.sorted(Comparator.comparing(Product::getName))
				.collect(Collectors.toList());
	}
	
	
	public List<Product> findAllByProductsParam(String q, BigDecimal minPrice, BigDecimal maxPrice) {
		
		return productRepository.findAll(ProductSpec.findByAttributes(q, minPrice, maxPrice));
	}
	
	
	@Transactional
	public Product update(Product product) {
		
		this.validateProductToUpdate(product);
		
		return productRepository.save(product);
	}
		

	@Transactional
	public void delete(Long id) {
		
		this.findById(id);
		
		productRepository.deleteById(id);
	}
	
	
	private void validateProductToSave(Product product) {
		
		this.validateProduct(product);
		
		if (!product.isNovo()) {
			throw new EntityAlreadySavedException(String.format("O produto %s já foi cadastrado e possui o ID %d",
					product.getName(), product.getId()));
		}
	}

	private void validateProductToUpdate(Product product) {
		
		this.validateProduct(product);
		
		if (product.isNovo()) {
			throw new EntityNotFoundException(String.format("O produto %s não foi cadastrado e não pode ser alterado",
					product.getName()));
		}
	}

	private void validateProduct(Product product) {
		if (product == null) {
			throw new ObjectException("Produto inválido!");
		} else {
			if (product.getName() == null || product.getName().trim().isEmpty()) {
				throw new ObjectException("Informe o nome do produto!");
			}
			if (product.getDescription() == null || product.getDescription().trim().isEmpty()) {
				throw new ObjectException("Informe a descrição do produto!");
			}
			if (product.getPrice() == null) {
				throw new ObjectException("Informe o preço do produto!");
			}
		}
	}
}
